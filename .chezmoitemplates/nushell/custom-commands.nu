export def l [pattern: glob = .] {
  ls --short-names $pattern | grid --color --icons --separator "  "
}

export def la [pattern: glob = .] {
  ls --all --short-names $pattern | grid --color --icons --separator "  "
}

export def --env y [...args] {
  let tmp = (mktemp -t "yazi-cwd.XXXXXX")
  yazi ...$args --cwd-file $tmp
  let cwd = (open $tmp)
  if $cwd != "" and $cwd != $env.PWD {
	  cd $cwd
  }
  rm -fp $tmp
}

export def committers [] {
  git log --no-merges --pretty=%aN | lines | histogram | sort-by count | reverse
}

export def backup-vault [] {
  let now = date now | format date "%Y-%m-%d %H:%M:%S"
  git add .
  git commit -m $"vault backup: ($now)"
  git push
}

export def remove-failed-commands-from-history [] {
  $nu.history-path | open | query db "DELETE FROM history WHERE exit_status != 0"
}

export def today [] {
  date now | format date "%Y-%m-%d"
}
