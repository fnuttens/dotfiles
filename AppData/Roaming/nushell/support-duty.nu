# Get support duty for the given string date.
# The command is expecting a `TEAM_CALENDAR` environment variable to point to an ICS file containing the data of interest.
# 
# ⚠️ This module depends on the nu_plugin_formats (https://github.com/nushell/nushell/tree/main/crates/nu_plugin_formats) plugin.
#
# Examples:
#   > support-duty today
#   > support-duty tomorrow
#   > support-duty 2025-05-02
export def main [date: string] {
  let formatted_date = $date | into datetime | format date "%Y-%m-%d"
  let find_results = all | find $formatted_date

  if ($find_results | length) == 0 {
    error make {
      msg: $"No support duty found on ($formatted_date). Please verify that it is a working day and that your calendar is up to date."
      label: {
        text: "Invalid date"
        span: (metadata $date).span
      }
    }
  }

  $find_results.0.duty
}

# Retrieve all support duties from ICS data
export def all [] {
  let support_duties = open $env.TEAM_CALENDAR
  | get events.0.properties
  | each { |it|
      $it | transpose --header-row --as-record | {
        date: ($in.DTSTART.0 | into datetime | format date "%Y-%m-%d")
        duty: ($in.SUMMARY.0 | parse "Support: {name}" | match $in {
          [$match] => $match.name,
          _ => null,
        })
      }
    }
  | where { |it| $it.duty != null }
  | sort-by date

  if (($support_duties | last | get date | into datetime) - (date now)) < 2wk {
    print "Warning: the last support duty found occurs in less than two weeks.\nConsider updating your ICS calendar."
  }

  $support_duties
}
