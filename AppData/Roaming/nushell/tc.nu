# Morning clock in
export def amin [] {
  let now = now-into-duration
  let data = data
  $data | upsert amin $now | to nuon | save -f $"($env.TC_PATH)/tc.nu"
  echo $"Clocked in at ($now). Have a nice day :)"
}

# Morning clock out
export def amout [] {
  let now = now-into-duration
  let data = data
  $data | upsert amout $now | to nuon | save -f $"($env.TC_PATH)/tc.nu"
  echo $"Clocked out at ($now). Enjoy your meal!"
}

# Afternoon clock in
export def pmin [] {
  let now = now-into-duration
  let data = data
  $data | upsert pmin $now | to nuon | save -f $"($env.TC_PATH)/tc.nu"
  echo $"Clocked in at ($now). Welcome back!"
}

# Afternoon clock out - updates the balance
export def pmout [
  --dry_run (-d): string # Simulates a clock out a given time (using the following format: 'Xh[Y]'). The balance won’t be updated.
] {
  let time_out = if $dry_run == null { now-into-duration } else { parse-time $dry_run false }
  let data = (data | upsert pmout $time_out)

  let new_balance = (compute-balance $data)
  let data = ($data | upsert balance $new_balance)

  if $dry_run == null {
    $data | to nuon | save -f $"($env.TC_PATH)/tc.nu"
  }

  echo $"Clocked out at ($time_out). Balance is now ($new_balance). See you tomorrow!"
}

# Shows the ideal afternoon clock out time, with or without the balance
export def leave [
  --midday (-m)       # Shows the ideal midday (noon) clock out time
  --ignore_break (-i) # Ignores the actual break time, taking the theoretical break into account instead
] {
  if $midday and $ignore_break {
    error make { msg: "Please choose one of the following flags: 'midday', 'ignore_break'" }
  }
  
  let data = data
  let daily_working_time = if $midday { $data.config.daily_working_time / 2 } else { $data.config.daily_working_time }
  let break = if $midday { 0min } else if $ignore_break { $data.config.break_time } else { $data.pmin - $data.amout }
  let leave_time = ($data.amin + $daily_working_time + $break)
  echo $"You can leave at ($leave_time), or ($leave_time - $data.balance) including the balance \(($data.balance)\)."
}

# Shows the ideal afternoon clock in time
export def break [] {
  let data = data
  let ideal_pmin = ($data.amout + $data.config.break_time)
  echo $"Clock in at ($ideal_pmin) to take a ($data.config.break_time) break."
}

# Sets a time
export def set [
  new_time: string,         # The new time to set, using the following format: 'Xh[Y]'
  --which (-w) = "balance", # Time to set ('balance' [default], 'amin', 'amout', 'pmin' or 'pmout')
  --negative (-n)           # Whether the time is negative or not. Only applicable with the balance.
] {
  let is_invalid_time = ($which != "balance" and $which != "amin" and $which != "amout" and $which != "pmin" and $which != "pmout")
  if $is_invalid_time {
    error make {
      msg: "Please type in a valid time to set: 'balance', 'amin', 'amout', 'pmin' or 'pmout'"
      label: {
        text: "Invalid input format"
        span: (metadata $which).span
      }
    }
  }

  let new_time = (parse-time $new_time $negative)
  let data = data
  $data | upsert $which $new_time | to nuon | save -f $"($env.TC_PATH)/tc.nu"
  echo $"($which) set to ($new_time)."
}

# Updates the balance according to today's working hours
export def balance [
  --dry (-d) # No balance update
] {
  let data = data
  let new_balance = (compute-balance $data)
  if $dry {
    echo $"The new balance would be set to ($new_balance)."
  } else {
    $data | upsert balance $new_balance | to nuon | save -f $"($env.TC_PATH)/tc.nu"
    echo $"Balance set to ($new_balance)."
  }
}

# Prints the time clock data (config + hours)
export def data [] {
  let path = $"($env.TC_PATH)/tc.nu"
  if not ($path | path exists) {
    create-default-data-file
  } else {
    open $"($env.TC_PATH)/tc.nu" | from nuon
  }
}

def now-into-duration [] {
  let now = (date now | into record)
  let now_hours = $now.hour | into duration --unit hr
  let now_minutes = $now.minute | into duration --unit min
  $now_hours + $now_minutes
}

def parse-time [time: string, is_negative: bool] {
  let parsed_time = ($time | parse "{hrs}h{mins}")

  if ($parsed_time | length) != 1 {
    error make {
      msg: "Please type in a time in the following format: 'Xh[Y]'",
      label: {
        text: "Invalid input format"
        span: (metadata $time).span
      }
    }
  }

  let parsed_time = $parsed_time.0
  let sign = if $is_negative { "-" } else { "" }
  mut hrs = $"($sign)($parsed_time.hrs)" | into int
  mut mins = $"($sign)($parsed_time.mins)" | if $in == "" { 0 } else { $in | into int }

  let hrs = ($hrs | into duration --unit hr)
  let mins = ($mins | into duration --unit min)
  $hrs + $mins
}

def compute-balance [data] {
  let total = (($data.amout - $data.amin) + ($data.pmout - $data.pmin))
  let offset = ($total - $data.config.daily_working_time)
  $data.balance + $offset
}

def create-default-data-file [] {
  let default_data = {
    config: {
      daily_working_time: (7hr + 48min)
      break_time: 45min
    }
    balance: 0min
    amin: 9hr
    amout: 12hr
    pmin: (12hr + 45min)
    pmout: 17hr
  }
  $default_data | to nuon | save -f $"($env.TC_PATH)/tc.nu"
  $default_data
}
